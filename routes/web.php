<?php

use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\Controller; 
use App\Http\Controllers\CostcenterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// TESTOWE //
Route::get('/raz',  function () {
    return 'Hello World';
});
/////////////

// START //
Route::get('/start',  function () {
    return view ('start');
})->name('start');
/////////////


// PANEL ADMINISTRACYJNY //
Route::get('/padmin',  function () {
    return view ('paneladmin');
})->name('padmin');
/////////////

//TABELA COSTCENTERS
Route::get('/costcenters', 'CostcenterController@index')->name('costcenters.index');
Route::get('/costcenters/{id}/show', 'CostcenterController@show')->name('costcenters.show');
Route::post('/costcenters/search', 'CostcenterController@search')->name('costcenters.search');
Route::get('/costcenters/create', 'CostcenterController@create')->name('costcenters.create');
Route::post('/costcenters/store', 'CostcenterController@store')->name('costcenters.store');
Route::get('/costcenters/{id}/edit', 'CostcenterController@edit')->name('costcenters.edit');
Route::put('/costcenters/{id}', 'CostcenterController@update')->name('costcenters.update');
Route::get('/costcenters/{id}/delete', 'CostcenterController@delete')->name('costcenters.delete');
Route::delete('/costcenters/{id}', 'CostcenterController@destroy')->name('costcenters.destroy');
Route::get('/costcenters/autocomplete', 'CostcenterController@autocomplete')->name('costcenters.autocomplete');

//TABELA USERS
Route::get('/users', 'UserController@index')->name('users.index');
Route::get('/users/{id}/show', 'UserController@show')->name('users.show');
Route::post('/users/search', 'UserController@search')->name('users.search');
Route::get('/users/create', 'UserController@create')->name('users.create');
Route::post('/users/store', 'UserController@store')->name('users.store');
Route::get('/users/{id}/edit', 'UserController@edit')->name('users.edit');
Route::put('/users/{id}', 'UserController@update')->name('users.update');
Route::get('/users/{id}/delete', 'UserController@delete')->name('users.delete');
Route::delete('/users/{id}', 'UserController@destroy')->name('users.destroy');
Route::get('/users/autocomplete', 'UserController@autocomplete')->name('users.autocomplete');

//TABELA USERGROUPS
Route::get('/usergroups', 'UsergroupController@index')->name('usergroups.index');
Route::get('/usergroups/{id}/show', 'UsergroupController@show')->name('usergroups.show');
Route::post('/usergroups/search', 'UsergroupController@search')->name('usergroups.search');
Route::get('/usergroups/create', 'UsergroupController@create')->name('usergroups.create');
Route::post('/usergroups/store', 'UsergroupController@store')->name('usergroups.store');
Route::get('/usergroups/{id}/edit', 'UsergroupController@edit')->name('usergroups.edit');
Route::put('/usergroups/{id}', 'UsergroupController@update')->name('usergroups.update');
Route::get('/usergroups/{id}/delete', 'UsergroupController@delete')->name('usergroups.delete');
Route::delete('/usergroups/{id}', 'UsergroupController@destroy')->name('usergroups.destroy');
Route::get('/usergroups/autocomplete', 'UsergroupController@autocomplete')->name('usergroups.autocomplete');

//TABELA STORAGEPLACES
Route::get('/storageplaces', 'StorageplaceController@index')->name('storageplaces.index');
Route::get('/storageplaces/{id}/show', 'StorageplaceController@show')->name('storageplaces.show');
Route::post('/storageplaces/search', 'StorageplaceController@search')->name('storageplaces.search');
Route::get('/storageplaces/create', 'StorageplaceController@create')->name('storageplaces.create');
Route::post('/storageplaces/store', 'StorageplaceController@store')->name('storageplaces.store');
Route::get('/storageplaces/{id}/edit', 'StorageplaceController@edit')->name('storageplaces.edit');
Route::put('/storageplaces/{id}', 'StorageplaceController@update')->name('storageplaces.update');
Route::get('/storageplaces/{id}/delete', 'StorageplaceController@delete')->name('storageplaces.delete');
Route::delete('/storageplaces/{id}', 'StorageplaceController@destroy')->name('storageplaces.destroy');
Route::get('/storageplaces/autocomplete', 'StorageplaceController@autocomplete')->name('storageplaces.autocomplete');


//TABELA CUBBIES
Route::get('/cubbies', 'CubbyController@index')->name('cubbies.index');
Route::get('/cubbies/{id}/show', 'CubbyController@show')->name('cubbies.show');
Route::post('/cubbies/search', 'CubbyController@search')->name('cubbies.search');
Route::get('/cubbies/create', 'CubbyController@create')->name('cubbies.create');
Route::post('/cubbies/store', 'CubbyController@store')->name('cubbies.store');
Route::get('/cubbies/{id}/edit', 'CubbyController@edit')->name('cubbies.edit');
Route::put('/cubbies/{id}', 'CubbyController@update')->name('cubbies.update');
Route::get('/cubbies/{id}/delete', 'CubbyController@delete')->name('cubbies.delete');
Route::delete('/cubbies/{id}', 'CubbyController@destroy')->name('cubbies.destroy');
Route::get('/cubbies/autocomplete', 'CubbyController@autocomplete')->name('cubbies.autocomplete');


//TABELA ITEMS
Route::get('/items', 'ItemController@index')->name('items.index');
Route::get('/items/{id}/show', 'ItemController@show')->name('items.show');
Route::post('/items/search', 'ItemController@search')->name('items.search');
Route::get('/items/create', 'ItemController@create')->name('items.create');
Route::post('/items/store', 'ItemController@store')->name('items.store');
Route::get('/items/{id}/edit', 'ItemController@edit')->name('items.edit');
Route::put('/items/{id}', 'ItemController@update')->name('items.update');
Route::get('/items/{id}/delete', 'ItemController@delete')->name('items.delete');
Route::delete('/items/{id}', 'ItemController@destroy')->name('items.destroy');
Route::get('/items/autocomplete', 'ItemController@autocomplete')->name('items.autocomplete');
