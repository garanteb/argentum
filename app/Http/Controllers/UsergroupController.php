<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use App\Models\Usergroup;
use App\Models\User;
use Illuminate\Http\Request;

class UsergroupController extends Controller
{
    public function index() {
        $usergroupsList = Usergroup::all();
        return view('usergroups.index',compact('usergroupsList'));
      }

      public function show($id) {
        $usergroup = Usergroup::find($id);
        if(!$usergroup) 
        {
          return redirect('usergroups');
        }
        return view('usergroups.show',compact('usergroup'));
      
      }

      public function search(Request $request) {
       $usergroupsList = Usergroup::where('name', 'like' , '%'.$request->name.'%')->get();
        return view('usergroups.index',compact('usergroupsList'));
      }

      public function create() {
        return view('usergroups.create');
      }

      public function store(Request $request) {
        
        $request->validate(['name'=>'required|unique:usergroups,name|max:40'],
        [
          'name.required'=>'nazwa jest wymagana!',
          'name.max'=>'maks. długość 40 znaków!',
          'name.unique'=>'nazwa musi być unikalna!'
        ]);
        $usergroup = Usergroup::create([
          'name'=>$request->name
           ]);

        return redirect('usergroups')->with('status','dodano nową grupę użytkownika: '.$usergroup->name);
      
      }

      public function edit($id) {
        $usergroup = Usergroup::find($id);
        if(!$usergroup) 
        {
          return redirect('usergroups');
        }
        return view('usergroups.edit',compact('usergroup'));
      }

      public function update( Request $request, $id) {
        $usergroup = Usergroup::find($id);
        $name = $request->name;
        if($usergroup->name==$name)
        {
          return view('usergroups.edit',compact('usergroup'));;
        }
        $request->validate(['name'=>'required|unique:usergroups,name|max:40'],
        [
          'name.required'=>'nazwa jest wymagana!',
          'name.max'=>'maks. długość 40 znaków!',
          'name.unique'=>'nazwa musi być unikalna!'
        ]);
        $usergroup = Usergroup::updateOrCreate(['id' => $id], ['name' => $name] );
        return redirect('usergroups')->with('status','zaktualizowano grupę użytkownika: ' .$usergroup->name );      
      }
       
      public function delete($id) {
        $usergroup = Usergroup::find($id);
        $user = User::where('groupId', $id)->get();
        $userCount = $user->count();

        if($usergroup)
        {
         return  view('usergroups.delete', compact('usergroup','user','userCount'));  

        }
        return redirect()->route('usergroups.index');
       }

      public function destroy($id) {
          $usergroup = Usergroup::find($id);
          if($usergroup)
          {
             $usergroup ->delete();
            return redirect('usergroups')->with('status','usunięto grupę użytkownika: '. $usergroup->name);
          }
          return redirect()->route('usergroups.index');
      }

      public function autocomplete(Request $request)
      {
          $data = Usergroup::select("name")
                  ->where("name","LIKE","%{$request->input('query')}%")
                  ->get();
     
          return response()->json($data);
      }
}
