<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DB;
use App\Models\Costcenter;
use Illuminate\Http\Request;

class CostcenterController extends Controller
{
      public function index() {
        $costcentersList = CostCenter::all();
        return view('costcenters.index',compact('costcentersList'));
      }

      public function show($id) {
        $costcenter = Costcenter::find($id);
        if(!$costcenter) 
        {
          return redirect('costcenters');
        }
        return view('costcenters.show',compact('costcenter'));
      }

      public function search(Request $request) {
       $costcentersList = Costcenter::where('name', 'like' , '%'.$request->name.'%')->get();
        return view('costcenters.index',compact('costcentersList'));
      }

      public function create() {
        return view('costcenters.create');
      }

      public function store(Request $request) {
        
        $request->validate(['name'=>'required|unique:costcenters|max:40'],
      [
        'name.required'=>'nazwa MPK jest wymagana!',
        'name.max'=>'maks. długość 40 znaków!',
        'name.unique'=>'nazwa musi być unikalna!'
      ]);
        $costcenter = Costcenter::create([
          'name'=>$request->name
           ]);

        return redirect('costcenters')->with('status','dodano nowe MPK: '.$costcenter->name);
      
      }

      public function edit($id) {
        $costcenter = Costcenter::find($id);
        if(!$costcenter) 
        {
          return redirect('costcenters');
        }
        return view('costcenters.edit',compact('costcenter'));
      }

      public function update( Request $request, $id) {
        $costcenter = Costcenter::find($id);
        $name = $request->name;
        if($costcenter->name==$name)
        {
          return view('costcenters.edit',compact('costcenter'));;
        }
        $request->validate(['name'=>'required|unique:costcenters|max:40'],
        [
          'name.required'=>'nazwa MPK jest wymagana!',
          'name.max'=>'maks. długość 40 znaków!',
          'name.unique'=>'nazwa musi być unikalna!'
        ]);
         $costcenter = Costcenter::updateOrCreate(['id' => $id], ['name' => $name] );
        return redirect('costcenters')->with('status','zaktualizowano MPK: ' .$costcenter->name );      
      }
       
      public function delete($id) {
        $costcenter = Costcenter::find($id);
        if($costcenter)
        {
         return  view('costcenters.delete', compact('costcenter'));  

        }
        return redirect()->route('costcenters.index');
       }

      public function destroy($id) {
          $costcenter = Costcenter::find($id);
          if($costcenter)
          {
            // ZMIENNA costcenter to odwołąnie do modelu/bazy danych - pozostaje w pamięci
            $costcenter ->delete();
            return redirect('costcenters')->with('status','usunięto MPK: '. $costcenter->name);
          }
          return redirect()->route('costcenters.index');
      }

      public function autocomplete(Request $request)
      {
          $data = Costcenter::select("name")
                  ->where("name","LIKE","%{$request->input('query')}%")
                  ->get();
     
          return response()->json($data);
      }
}
