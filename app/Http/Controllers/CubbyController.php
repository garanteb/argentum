<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Cubby;

class CubbyController extends Controller
{
    public function index() {
        $cubbiesList = Cubby::all();
        return view('cubbies.index',['cubbiesList'=>$cubbiesList]);
      }

      public function show($id) {
        $cubby = Cubby::find($id);
        if(!$cubby) 
        {
          return redirect('cubbies');
        }
        return view('cubbies.show',['cubby'=>$cubby]);
      }

      public function search(Request $request) {
       $cubbiesList = Cubby::where('name', 'like' , '%'.$request->name.'%')->get();
        return view('cubbies.index',['cubbiesList'=>$cubbiesList]);
      }

      public function create() {
        return view('cubbies.create');
      }

      public function store(Request $request) {
        
        $request->validate(['name'=>'required|unique:cubbies|max:40']);
        $cubby = Cubby::create([
          'name'=>$request->name
           ]);

        return redirect('cubby')->with('status','dodano nowe CUBBY: '.$cubby->name);
      
      }

      public function edit($id) {
        $cubby = Cubby::find($id);
        if(!$cubby) 
        {
          return redirect('cubbies');
        }
        return view('cubbies.edit',['cubby'=>$cubby]);
      }

      public function update( Request $request, $id) {
        $cubby = Cubby::find($id);
        $name = $request->name;
        if($cubby->name==$name)
        {
          return view('cubbies.edit',compact('cubby'));;
        }
        $request->validate(['name'=>'required|unique:cubbies|max:40']);
        $cubby = Cubby::updateOrCreate(['id' => $id], ['name' => $name] );
        return redirect('cubbies')->with('status','zaktualizowano Cubby: ' .$cubby->name );      
      }
       
      public function delete($id) {
        $cubby = Cubby::find($id);
        if($cubby)
        {
         return  view('cubbies.delete', compact('cubby'));  

        }
        return redirect()->route('cubbies.index');
       }

      public function destroy($id) {
          $cubby = Cubby::find($id);
          if($cubby)
          {
            $cubby ->delete();
            return redirect('cubbies')->with('status','usunięto Cubby '. $cubby->name);
          }
          return redirect()->route('cubbies.index');
      }

      public function autocomplete(Request $request)
      {
          $data = Cubby::select("name")
                  ->where("name","LIKE","%{$request->input('query')}%")
                  ->get();
     
          return response()->json($data);
      }
}
