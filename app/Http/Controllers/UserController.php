<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use DB;
use App\Models\User;
use App\Models\Usergroup;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function index() {
     //  $usersList = User::all();

     $usersList = DB::table('users')
     ->join('usergroups', 'usergroups.id', '=', 'users.groupId')
     ->select('users.id', 'users.name as userName','usergroups.name as usergroupName')
     ->get();

      // dd($usersList);

        return view('users.index',compact('usersList'));
      }

      public function show($id) {
        $user = User::find($id);
        if(!$user){
          return redirect('users');
        }
        return view('users.show',compact('user'));  

      }

      public function search(Request $request) {
      // $usersList = User::where('name', 'like' , '%'.$request->name.'%')->get();
       $usersList = DB::table('users')
       ->join('usergroups', 'usergroups.id', '=', 'users.groupId')
       ->select('users.id', 'users.name as userName','usergroups.name as usergroupName')
       ->where('users.name', 'like' , '%'.$request->name.'%')
       ->get();
        return view('users.index',compact('usersList'));
      }

      public function create() {
        $usergroups = Usergroup::all();
        return view('users.create',compact('usergroups'));
      }

      public function store(Request $request) {
 

        $request->validate(['name'=>'required|max:40',
        'login'=>'required|unique:users,login|max:20',
        'password'=>'required|max:30',
        'department'=>'max:30',
        'accessAdmin'=>'required',
      ],[
        'name.required'=>'imię i nazwisko to pole jest wymagane!',
        'name.max'=>'nazwa maks. dł 40 znaków!',
        'login.required'=>'login jest wymagany!',
        'login.max'=>'login maks. dł 20 znaków!',
        'login.unique'=>'login jest używany!',
        'password.required'=>'hasło jest wymagane!',
        'password.max'=>'hasło maks. dł 30 znaków!',
        'department.max'=>'wydział maks. dł 30 znaków!'
      ]);

         $user = User::create([
          'name'=>$request->name,
          'login'=>$request->login,
          'department'=>$request->department,
          'accessAdmin'=>$request->accessAdmin,
          'email'=>$request->email,
          'password'=>$request->password,
          'groupId'=>$request->groupId,
           ],[
        'name.required'=>'imię i nazwisko to pole jest wymagane!',
        'name.max'=>'nazwa maks. dł 40 znaków!',
        'login.required'=>'login jest wymagany!',
        'login.max'=>'login maks. dł 20 znaków!',
        'login.unique'=>'login jest używany!',
        'password.required'=>'hasło jest wymagane!',
        'password.max'=>'hasło maks. dł 30 znaków!',
        'department.max'=>'wydział maks. dł 30 znaków!'
      ]);

        return redirect('users')->with('status','dodano nowego USERA: '.$user->name);
      
      }

      public function edit($id) {
        $user = User::find($id);
        if($user)
        {
          $usergroups = Usergroup::all();
          return view('users.edit',compact('user','usergroups'));
        }
        return redirect()->route('users.index');
      }

      public function update( Request $request, $id) {
        $user = User::find($id);
 
        $request->validate(['name'=>'required|max:40',
        'login'=>'required|unique:users,login,'.$user->id.'|max:20,',
        'password'=>'required|max:30',
        'department'=>'max:30',
        'accessAdmin'=>'required',
      ],[
        'name.required'=>'imię i nazwisko to pole jest wymagane!',
        'name.max'=>'nazwa maks. dł 40 znaków!',
        'login.required'=>'login jest wymagany!',
        'login.max'=>'login maks. dł 20 znaków!',
        'login.unique'=>'login jest używany!',
        'password.required'=>'hasło jest wymagane!',
        'password.max'=>'hasło maks. dł 30 znaków!',
        'department.max'=>'wydział maks. dł 30 znaków!',
      ]);

        $user = User::updateOrCreate(['id' => $id], 
        ['name'=>$request->name,
        'login'=>$request->login,
        'department'=>$request->department,
        'accessAdmin'=>$request->accessAdmin,
        'email'=>$request->email,
        'password'=>$request->password,
        'groupId'=>$request->groupId] );
       
        return redirect('users')->with('status','zaktualizowano USERA: ' .$user->name );      
      }
       
      public function delete($id) {
        $user = User::find($id);
        if($user)
        {
         return  view('users.delete', compact('user'));  
        }
        return redirect()->route('users.index');
       }

      public function destroy($id) {
          $user = User::find($id);
          if($user)
          {
            $user ->delete();
            return redirect('users')->with('status','usunięto USERA: '. $user->name);
          }
          return redirect()->route('users.index');
      }

      public function autocomplete(Request $request)
      {
          $data = User::select("name")
                  ->where("name","LIKE","%{$request->input('query')}%")
                  ->get();
     
          return response()->json($data);
      }
}
