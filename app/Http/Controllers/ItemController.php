<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Item;

class ItemController extends Controller
{
    public function index() {
        $itemsList = Item::all();
        return view('items.index',compact('itemsList'));
      }

      public function show($id) {
        $item = Item::find($id);
        if(!$item) 
        {
          return redirect('items');
        }
        return view('items.show',compact('item'));
      }

      public function search(Request $request) {
       $itemsList = Item::where('name1', 'like' , '%'.$request->name.'%')
            ->orWhere('name2', 'like' , '%'.$request->name.'%')
            ->orWhere('name3', 'like' , '%'.$request->name.'%')
            ->orWhere('barcode', 'like' , '%'.$request->name.'%')
            ->get();
   
        return view('items.index',compact('itemsList'));
      }

      public function create() {
        return view('items.create');
      }

      public function store(Request $request) {
        
        $request->validate([
          'name1'=>'required|unique:items|max:80',
          'name2'=>'max:80',
          'name3'=>'max:80',
          'barcode'=>'required|unique:items|max:40',
          'height'=>'required|numeric|between:0,16777215',
          'width'=>'required|numeric|between:0,16777215',
          'length'=>'required|numeric|between:0,16777215',
          'weight'=>'required|numeric',
          'boxAmount'=>'required|numeric|between:0,16777215',
          'minInv'=>'required|numeric|between:0,16777215',
          'currInv'=>'required|numeric|between:0,16777215',
          'active'=>'required'
        ],
        [
          'name1.required'=>'nazwa1 jest wymagana!',
          'name1.unique'=>'nazwa1 musi być unikalna',
          'name1.max'=>'name1 maks dł. 80 znaków',
          'name1.required'=>'nazwa1 jest wymagana!',
          'name2.max'=>'name2 maks dł. 80 znaków',
          'name3.max'=>'name3 maks dł. 80 znaków',
          'barcode.required'=>'barcode jest wymagany!',
          'barcode.unique'=>'barcode musi być unikalny',
          'barcode.max'=>'maks dł. 40 znaków',
          'height.required'=>'wysokość jest wymagany!',
          'height.numeric'=>'wysokość musi być liczbą',
          'height.between'=>'wartość między 0 a 16777215',
          'width.required'=>'szerokość jest wymagana!',
          'width.numeric'=>'szerokość musi być liczbą',
          'width.between'=>'szerokość między 0 a 16777215',
          'length.required'=>'długość jest wymagana!',
          'length.numeric'=>'długość musi być liczbą',
          'length.between'=>'długość między 0 a 16777215',
          'weight.required'=>'waga jest wymagana!',
          'weight.numeric'=>'waga musi być liczbą',
          'minInv.required'=>'stan min. jest wymagana!',
          'minInv.numeric'=>'stan min. musi być liczbą',
          'minInv.between'=>'stan min. między 0 a 16777215',
          'currInv.required'=>'stan aktualny jest wymagana!',
          'currInv.numeric'=>'stan aktualny musi być liczbą',
          'currInv.between'=>'stan aktualny między 0 a 16777215',
          'boxAmount.required'=>'opak. zbiorcze jest wymagana!',
          'boxAmount.numeric'=>'opak. zbiorcze musi być liczbą',
          'boxAmount.between'=>'opak. zbiorcze między 0 a 16777215',
        ]
      );
        $item = Item::create([
          'name1'=>$request->name1,
          'name2'=>$request->name2,
          'name3'=>$request->name3,
          'barcode'=>$request->barcode,
          'height'=>$request->height,
          'width'=>$request->width,
          'length'=>$request->length,
          'weight'=>$request->weight,
          'boxAmount'=>$request->boxAmount,
          'minInv'=>$request->minInv,
          'currInv'=>$request->currInv,
          'picPath'=>$request->picPath,
          'dateActive'=>$request->dateActive,
          'active'=>$request->active,
           ]);

        return redirect('items')->with('status','dodano nowy artykuł: '.$item->name1);
      
      }

      public function edit($id) {
        $item = Item::find($id);
        if(!$item) 
        {
          return redirect('items');
        }
        return view('items.edit',compact('item'));
      }

      public function update( Request $request, $id) {
        $item = Item::find($id);

        $request->validate([
          'name1'=>'required|unique:items,name1,'.$item->id.'|max:80',
          'name2'=>'max:80',
          'name3'=>'max:80',
          'barcode'=>'required|unique:items,barcode,'.$item->id.'|max:40',
          'height'=>'required|numeric|between:0,16777215',
          'width'=>'required|numeric|between:0,16777215',
          'length'=>'required|numeric|between:0,16777215',
          'weight'=>'required|numeric',
          'boxAmount'=>'required|numeric|between:0,16777215',
          'minInv'=>'required|numeric|between:0,16777215',
          'currInv'=>'required|numeric|between:0,16777215',
          'active'=>'required'
          ]
          ,
          [
            'name1.required'=>'nazwa1 jest wymagana!',
            'name1.unique'=>'nazwa1 musi być unikalna',
            'name1.max'=>'name1 maks dł. 80 znaków',
            'name1.required'=>'nazwa1 jest wymagana!',
            'name2.max'=>'name2 maks dł. 80 znaków',
            'name3.max'=>'name3 maks dł. 80 znaków',
            'barcode.required'=>'barcode jest wymagany!',
            'barcode.unique'=>'barcode musi być unikalny',
            'barcode.max'=>'maks dł. 40 znaków',
            'height.required'=>'wysokość jest wymagany!',
            'height.numeric'=>'wysokość musi być liczbą',
            'height.between'=>'wartość między 0 a 16777215',
            'width.required'=>'szerokość jest wymagana!',
            'width.numeric'=>'szerokość musi być liczbą',
            'width.between'=>'szerokość między 0 a 16777215',
            'length.required'=>'długość jest wymagana!',
            'length.numeric'=>'długość musi być liczbą',
            'length.between'=>'długość między 0 a 16777215',
            'weight.required'=>'waga jest wymagana!',
            'weight.numeric'=>'waga musi być liczbą',
            'minInv.required'=>'stan min. jest wymagana!',
            'minInv.numeric'=>'stan min. musi być liczbą',
            'minInv.between'=>'stan min. między 0 a 16777215',
            'currInv.required'=>'stan aktualny jest wymagana!',
            'currInv.numeric'=>'stan aktualny musi być liczbą',
            'currInv.between'=>'stan aktualny między 0 a 16777215',
            'boxAmount.required'=>'opak. zbiorcze jest wymagana!',
            'boxAmount.numeric'=>'opak. zbiorcze musi być liczbą',
            'boxAmount.between'=>'opak. zbiorcze między 0 a 16777215',
          ]);

          $item = Item::updateOrCreate(['id' => $id], 
          [    
          'name1'=>$request->name1,
          'name2'=>$request->name2,
          'name3'=>$request->name3,
          'barcode'=>$request->barcode,
          'height'=>$request->height,
          'width'=>$request->width,
          'length'=>$request->length,
          'weight'=>$request->weight,
          'boxAmount'=>$request->boxAmount,
          'minInv'=>$request->minInv,
          'currInv'=>$request->currInv,
          'picPath'=>$request->picPath,
          'dateActive'=>$request->dateActive,
          'active'=>$request->active,
          ] 
        );
     
        return redirect('items')->with('status','zaktualizowano artykuł: ' .$item->name1 );      
      }
       
      public function delete($id) {
        $item = Item::find($id);
        if($item)
        {
         return  view('items.delete', compact('item'));  

        }
        return redirect()->route('items.index');
       }

      public function destroy($id) {
          $item = Item::find($id);
          if($item)
          {
            $item ->delete();
            return redirect('items')->with('status','usunięto artykuł:'. $item->name1);
          }
          return redirect()->route('items.index');
      }

      public function autocomplete(Request $request)
      {
           // poprawić
          $data= Item::Select("name1")
              ->where("name1","like","%{$request->input('query')}%")
              ->orWhere('name2', 'like' ,"%{$request->input('query')}%")
              ->orWhere('name3', 'like' , "%{$request->input('query')}%")
              ->get();
          return response()->json($data);
      }
}
