<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Storageplace;


class StorageplaceController extends Controller
{
    public function index() {
        $storageplacesList = Storageplace::all();
        return view('storageplaces.index',compact('storageplacesList'));
      }

      public function show($id) {
        $storageplace = Storageplace::find($id);
        if(!$storageplace) 
        {
          return redirect('storageplaces');
        }
        return view('storageplaces.show',compact('storageplace'));
      }

      public function search(Request $request) {
        $storageplacesList = Storageplace::where('name', 'like' , '%'.$request->name.'%')
        ->orWhere('barcode', 'like' , '%'.$request->name.'%')
        ->get();

        return view('storageplaces.index',compact('storageplacesList'));
      }

      public function create() {
        return view('storageplaces.create');
      }

      public function store(Request $request) {
        
        $request->validate([
          'name'=>'required|max:30',
          'barcode'=>'required|unique:storageplaces|max:40',
          'stillageNo'=>'required|numeric|between:0,255',
          'shelfNo'=>'required|numeric|between:0,255',
          'placeNo'=>'required|numeric|between:0,255',
          'lane'=>'required|numeric|between:0,255',
          'accessTime'=>'required|numeric|between:0,255',
          'height'=>'required|numeric|between:0,65535',
          'width'=>'required|numeric|between:0,65535',
          'length'=>'required|numeric|between:0,65535',
          'loadMax'=>'required|numeric|between:0,65535',
          'maxAmountOfItems'=>'required|numeric|between:0,65535',
          'active'=>'required'
          ]
          ,
          [
            'name.required'=>'nazwa jest wymagana!',
            'stillageNo.required'=>'nr regału jest wymagany!',
            'stillageNo.numeric'=>'nr regału musi być liczbą',
            'stillageNo.between'=>'nr regału między 0 a 255',
            'shelfNo.required'=>'nr półki jest wymagany!',
            'shelfNo.numeric'=>'nr półki musi być liczbą',
            'shelfNo.between'=>'nr półki między 0 a 255',
            'placeNo.required'=>'nr miejsca jest wymagany!',
            'placeNo.numeric'=>'nr miejsca musi być liczbą',
            'placeNo.between'=>'nr miejsca między 0 a 255',
            'lane.required'=>'nr alejki jest wymagany!',
            'lane.numeric'=>'nr alejki musi być liczbą',
            'lane.between'=>'nr alejki między 0 a 255',
            'accessTime.required'=>'czas dostępu jest wymagany!',
            'accessTime.numeric'=>'czas dostępu musi być liczbą',
            'accessTime.between'=>'czas dostępu między 0 a 255',
            'height.required'=>'wysokość jest wymagana!',
            'height.numeric'=>'wysokość musi być liczbą',
            'height.between'=>'wysokość między 0 a 255',
            'width.required'=>'szerokość jest wymagana!',
            'width.numeric'=>'szerokość musi być liczbą',
            'width.between'=>'szerokość między 0 a 255',
            'length.required'=>'długość jest wymagana!',
            'length.numeric'=>'długość musi być liczbą',
            'length.between'=>'długość między 0 a 255',
            'loadMax.required'=>'maks. obciążenie jest wymagana!',
            'loadMax.numeric'=>'maks. obciążenie musi być liczbą',
            'loadMax.between'=>'maks. obciążenie między 0 a 255',
            'maxAmountOfItems.required'=>'maks liczba różnych  artykułów jest wymagana!',
            'maxAmountOfItems.numeric'=>'maks liczba różnych  artykułów musi być liczbą',
            'maxAmountOfItems.between'=>'maks liczba różnych  artykułów między 0 a 255',
          ]
        
        );
        $storageplace = Storageplace::create([
          'name'=>$request->name,
          'barcode'=>$request->barcode,
          'stillageNo'=>$request->stillageNo,
          'shelfNo'=>$request->shelfNo,
          'placeNo'=>$request->placeNo,
          'lane'=>$request->lane,
          'accessTime'=>$request->accessTime,
          'height'=>$request->height,
          'width'=>$request->width,
          'length'=>$request->length,
          'loadMax'=>$request->loadMax,
          'loadCurrent'=>$request->loadCurrent,
          'maxAmountOfItems'=>$request->maxAmountOfItems,
          'active'=>$request->active,
           ]);

        return redirect('storageplaces')->with('status','dodano nowy regał: '.$storageplace->name);
      
      }

      public function edit($id) {
        $storageplace = Storageplace::find($id);
        if(!$storageplace) 
        {
          return redirect('storageplaces');
        }
        return view('storageplaces.edit',compact('storageplace'));
      }

      public function update( Request $request, $id) {
        $storageplace = Storageplace::find($id);
         $request->validate([
            'name'=>'required|max:30',
            'barcode'=>'required|unique:storageplaces,barcode,'.$storageplace->id.'|max:40',
            'stillageNo'=>'required|numeric|between:0,255',
            'shelfNo'=>'required|numeric|between:0,255',
            'placeNo'=>'required|numeric|between:0,255',
            'lane'=>'required|numeric|between:0,255',
            'accessTime'=>'required|numeric|between:0,255',
            'height'=>'required|numeric|between:0,65535',
            'width'=>'required|numeric|between:0,65535',
            'length'=>'required|numeric|between:0,65535',
            'loadMax'=>'required|numeric|between:0,65535',
            'maxAmountOfItems'=>'required|numeric|between:0,65535',
            'active'=>'required'
            ]
            ,
            [
              'name.required'=>'nazwa jest wymagana!',
              'stillageNo.required'=>'nr regału jest wymagany!',
              'stillageNo.numeric'=>'nr regału musi być liczbą',
              'stillageNo.between'=>'nr regału między 0 a 255',
              'shelfNo.required'=>'nr półki jest wymagany!',
              'shelfNo.numeric'=>'nr półki musi być liczbą',
              'shelfNo.between'=>'nr półki między 0 a 255',
              'placeNo.required'=>'nr miejsca jest wymagany!',
              'placeNo.numeric'=>'nr miejsca musi być liczbą',
              'placeNo.between'=>'nr miejsca między 0 a 255',
              'lane.required'=>'nr alejki jest wymagany!',
              'lane.numeric'=>'nr alejki musi być liczbą',
              'lane.between'=>'nr alejki między 0 a 255',
              'accessTime.required'=>'czas dostępu jest wymagany!',
              'accessTime.numeric'=>'czas dostępu musi być liczbą',
              'accessTime.between'=>'czas dostępu między 0 a 255',
              'height.required'=>'wysokość jest wymagana!',
              'height.numeric'=>'wysokość musi być liczbą',
              'height.between'=>'wysokość między 0 a 255',
              'width.required'=>'szerokość jest wymagana!',
              'width.numeric'=>'szerokość musi być liczbą',
              'width.between'=>'szerokość między 0 a 255',
              'length.required'=>'długość jest wymagana!',
              'length.numeric'=>'długość musi być liczbą',
              'length.between'=>'długość między 0 a 255',
              'loadMax.required'=>'maks. obciążenie jest wymagana!',
              'loadMax.numeric'=>'maks. obciążenie musi być liczbą',
              'loadMax.between'=>'maks. obciążenie między 0 a 255',
              'maxAmountOfItems.required'=>'maks liczba różnych  artykułów jest wymagana!',
              'maxAmountOfItems.numeric'=>'maks liczba różnych  artykułów musi być liczbą',
              'maxAmountOfItems.between'=>'maks liczba różnych  artykułów między 0 a 255',
            ]);

          $storageplace = Storageplace::updateOrCreate(['id' => $id], 
          [    
            'name'=>$request->name,
            'barcode'=>$request->barcode,
            'stillageNo'=>$request->stillageNo,
            'shelfNo'=>$request->shelfNo,
            'placeNo'=>$request->placeNo,
            'lane'=>$request->lane,
            'accessTime'=>$request->accessTime,
            'height'=>$request->height,
            'width'=>$request->width,
            'length'=>$request->length,
            'loadMax'=>$request->loadMax,
            'loadCurrent'=>$request->loadCurrent,
            'maxAmountOfItems'=>$request->maxAmountOfItems,
            'active'=>$request->active]
        );
     

        return redirect('storageplaces')->with('status','zaktualizowano regał: ' .$storageplace->name );      
      }
      // TRZEBA POPRAWIĆ 

      public function delete($id) {
        $storageplace = Storageplace::find($id);
        if($storageplace)
        {
         return  view('storageplaces.delete', compact('storageplace'));  

        }
        return redirect()->route('storageplaces.index');
       }

      public function destroy($id) {
          $storageplace = Storageplace::find($id);
          if($storageplace)
          {
            $storageplace ->delete();
            return redirect('storageplaces')->with('status','usunięto regał:'. $storageplace->name);
          }
          return redirect()->route('storageplaces.index');
      }

      public function autocomplete(Request $request)
      {

        // TA WERSJA NIE DZIAŁA
         // $data = Storageplace::Where("name","LIKE","%{$request->input('query')}%")
         //         ->orWhere("barcode","LIKE","%{$request->input('query')}%")
         //         ->get();
     
         // TA WERSJA NIE DZIAŁA
         //    $names = Storageplace::Where("name","LIKE","%{$request->input('query')}%")->get();
         //    $barcodes = Storageplace::Where("barcode","LIKE","%{$request->input('query')}%")->get();
         //    $data = $names->union($barcodes);
           
          $data= Storageplace::Select("name")
               ->Where("name","LIKE","%{$request->input('query')}%")
               ->get();
     

          return response()->json($data);
      }

}
