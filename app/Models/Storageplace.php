<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Storageplace extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'name',
        'barcode',
        'stillageNo',
        'shelfNo',
        'placeNo',
        'lane',
        'accessTime',
        'height',
        'width',
        'length',
        'loadMax',
        'loadCurrent',
        'maxAmountOfItems',
        'active'
    ];

}
