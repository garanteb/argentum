<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStorageplacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storageplaces', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',30);
            $table->string('barcode',40)->unique();
            $table->tinyInteger('stillageNo')->unsigned();
            $table->tinyInteger('shelfNo')->unsigned();
            $table->tinyInteger('placeNo')->unsigned();
            $table->tinyInteger('lane')->unsigned();
            $table->tinyInteger('accessTime')->unsigned();
            $table->mediumInteger('height')->default('99999')->unsigned();
            $table->mediumInteger('width')->default('99999')->unsigned();
            $table->mediumInteger('length')->default('99999')->unsigned();
            $table->mediumInteger('loadMax')->default('99999')->unsigned();
            $table->mediumInteger('loadCurrent')->default('0')->unsigned();
            $table->smallInteger('maxAmountOfItems')->default('65535')->unsigned();
            $table->boolean('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storageplaces');
    }
}
