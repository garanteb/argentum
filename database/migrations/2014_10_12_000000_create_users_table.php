<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('login',20)->unique();
            $table->string('name',40);
            $table->string('department',30)->nullable();
            $table->boolean('accessAdmin');
            $table->string('email')->nullable();
            $table->string('password',30);
             // KLUCZ OBCY DO USERGROUPS 
            $table->integer('groupId')->unsigned();
            $table->foreign('groupId')->references('id')->on('usergroups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
