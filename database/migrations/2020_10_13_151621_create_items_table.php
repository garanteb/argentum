<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name1',80)->unique();
            $table->string('name2',80)->nullable();
            $table->string('name3',80)->nullable();
            $table->string('barcode',40)->unique();
            $table->mediumInteger('height')->default('1')->unsigned();
            $table->mediumInteger('width')->default('1')->unsigned();
            $table->mediumInteger('length')->default('1')->unsigned();
            $table->decimal('weight',6,3)->default('1.00')->unsigned();
            $table->mediumInteger('boxAmount')->default('1')->unsigned();
            $table->mediumInteger('minInv')->default('1')->unsigned();
            $table->mediumInteger('currInv')->default('1')->unsigned();
            $table->string('picPath')->nullable();
            $table->string('dateActive')->nullable();
            $table->boolean('active');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
