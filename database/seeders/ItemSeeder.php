<?php

namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('items')->insert([ [
            'name1'=>'KORPUS PF2*52/PF2*46',
            'name2'=>'MITSUBISHI G10741',
            'name3'=>'22.A432../22.Y',
            'barcode'=>'1027573',
            'height'=>'500',
            'width'=>'800',
            'length'=>'1200',
            'weight'=>'1',
            'boxAmount'=>'1',
            'minInv'=>'1',
            'currInv'=>'1',
            'picPath'=>'obrazek19999.jpg',
            'dateActive'=>'20250101',
            'active'=>'1'
        ],
        [
            'name1'=>'KORPUS PFC1*52',
            'name2'=>'IVECO G4998',
            'name3'=>'22.7271',
            'barcode'=>'1029973',
            'height'=>'500',
            'width'=>'800',
            'length'=>'1200',
            'weight'=>'11',
            'boxAmount'=>'1',
            'minInv'=>'1',
            'currInv'=>'1',
            'picPath'=>'obrazek19999.jpg',
            'dateActive'=>'20250101',
            'active'=>'1'
        ]
        ,
        [
            'name1'=>'KORPUS PF2*44 EVO ANT (POZIOMA )',
            'name2'=>'MAZAK H4 G5709',
            'name3'=>'22.7774',
            'barcode'=>'7097333',
            'height'=>'500',
            'width'=>'800',
            'weight'=>'20',
            'length'=>'1200',
            'boxAmount'=>'1',
            'minInv'=>'1',
            'currInv'=>'1',
            'picPath'=>'obrazek19999.jpg',
            'dateActive'=>'20250101',
            'active'=>'1'
        ]
        ,
        [
            'name1'=>'PIATTO 190',
            'name2'=>'IVECO KPL G10376',
            'name3'=>'27.7705/06',
            'barcode'=>'7097233',
            'height'=>'500',
            'width'=>'800',
            'length'=>'1200',
            'weight'=>'8.99',
            'boxAmount'=>'1',
            'minInv'=>'1',
            'currInv'=>'1',
            'picPath'=>'obrazek19999.jpg',
            'dateActive'=>'20250101',
            'active'=>'1'
        ]
        ,
        [
            'name1'=>'BIDISCO PUMA',
            'name2'=>'MORI SEIKI G5746',
            'name3'=>'22.7621/22',
            'barcode'=>'9097230',
            'height'=>'500',
            'width'=>'800',
            'length'=>'1200',
            'weight'=>'0.95',
            'boxAmount'=>'1',
            'minInv'=>'1',
            'currInv'=>'1',
            'picPath'=>'obrazek19999.jpg',
            'dateActive'=>'20250101',
            'active'=>'1'
        ]]);
    }
}
