<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class StorageplaceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('storageplaces')->insert([ [
            'name'=>'Regał 101',
            'barcode'=>'1027573',
            'stillageNo'=>'1',
            'shelfNo'=>'1',
            'placeNo'=>'1',
            'lane'=>'1',
            'accessTime'=>'10',
            'height'=>'1',
            'width'=>'1',
            'length'=>'1',
            'loadMax'=>'65535',
            'loadCurrent'=>'0',
            'maxAmountOfItems'=>'65535',
            'active'=>'1'
        ],
        [
            'name'=>'Regał 101',
            'barcode'=>'1155784',
            'stillageNo'=>'1',
            'shelfNo'=>'2',
            'placeNo'=>'1',
            'lane'=>'1',
            'accessTime'=>'20',
            'height'=>'1',
            'width'=>'1',
            'length'=>'1',
            'loadMax'=>'65535',
            'loadCurrent'=>'0',
            'maxAmountOfItems'=>'65535',
            'active'=>'1'
        ],
        [
            'name'=>'Regał 101',
            'barcode'=>'1031563',
            'stillageNo'=>'1',
            'shelfNo'=>'3',
            'placeNo'=>'1',
            'lane'=>'1',
            'accessTime'=>'30',
            'height'=>'1',
            'width'=>'1',
            'length'=>'1',
            'loadMax'=>'65535',
            'loadCurrent'=>'0',
            'maxAmountOfItems'=>'65535',
            'active'=>'1'
        ],
        [
            'name'=>'Regał 102',
            'barcode'=>'1118159',
            'stillageNo'=>'1',
            'shelfNo'=>'1',
            'placeNo'=>'1',
            'lane'=>'1',
            'accessTime'=>'10',
            'height'=>'1',
            'width'=>'1',
            'length'=>'1',
            'loadMax'=>'65535',
            'loadCurrent'=>'0',
            'maxAmountOfItems'=>'65535',
            'active'=>'1'
        ],
        [
            'name'=>'Regał 102',
            'barcode'=>'1079298',
            'stillageNo'=>'1',
            'shelfNo'=>'2',
            'placeNo'=>'1',
            'lane'=>'1',
            'accessTime'=>'20',
            'height'=>'1',
            'width'=>'1',
            'length'=>'1',
            'loadMax'=>'65535',
            'loadCurrent'=>'0',
            'maxAmountOfItems'=>'65535',
            'active'=>'1'
        ],
        [
            'name'=>'Regał 102',
            'barcode'=>'1126587',
            'stillageNo'=>'1',
            'shelfNo'=>'3',
            'placeNo'=>'1',
            'lane'=>'1',
            'accessTime'=>'30',
            'height'=>'1',
            'width'=>'1',
            'length'=>'1',
            'loadMax'=>'65535',
            'loadCurrent'=>'0',
            'maxAmountOfItems'=>'65535',
            'active'=>'1'
        ]]);
    }
}
