<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([ [
            'login'=>'JeAlbr',
            'name'=>'Jens Albrecht',
            'department'=>'Hala1',
            'accessAdmin'=>'0',
            'email'=>'jensalbrecht@gmail.com',
            'password'=>'jerijcoiermijorij',
            'groupId'=>'1'
        ],
        [
            'login'=>'MBellaMi1769',
            'name'=>'Michael Bella',
            'department'=>'Hala 14',
            'accessAdmin'=>'0',
            'email'=>'michaelbella@gmail.com',
            'password'=>'764732668778776jnednewlndiow',
            'groupId'=>'2'
        ],
        [
            'login'=>'WyCl',
            'name'=>'Will Clye',
            'department'=>'Hala 35 ',
            'accessAdmin'=>'1',
            'email'=>'willclye@gmail.com',
            'password'=>'008lkjdjiojwco',
            'groupId'=>'1'
        ]
        ,
        [
            'login'=>'Ks9088%',
            'name'=>'Karl Schmitt',
            'department'=>'Hala 316 ',
            'accessAdmin'=>'1',
            'email'=>'karlschmitt@gmail.com',
            'password'=>'008lkjdjiojwco',
            'groupId'=>'3'
        ]
        ,
        [
            'login'=>'BH19990',
            'name'=>'Heinrich Best',
            'department'=>'Magazyn ',
            'accessAdmin'=>'1',
            'email'=>'heinrichbest@gmail.com',
            'password'=>'008lkjdjiojwco',
            'groupId'=>'3'
        ]
        ,
        [
            'login'=>'ND@@@kel',
            'name'=>'Nils Dunkel',
            'department'=>'Magazyn ',
            'accessAdmin'=>'1',
            'email'=>'nilsdunkel@gmail.com',
            'password'=>'00####3djio99)co',
            'groupId'=>'2'
        ]
        ,
        [
            'login'=>'Safk&&7',
            'name'=>'Mika Saefken',
            'department'=>'Magazyn ',
            'accessAdmin'=>'1',
            'email'=>'mikasaefken@gmail.com',
            'password'=>'99966',
            'groupId'=>'1'
        ],
        [
            'login'=>'Adamski 1999',
            'name'=>'Herbert Adamski',
            'department'=>'Hala 14',
            'accessAdmin'=>'0',
            'email'=>'herbertadamski@gmail.com',
            'password'=>'764732wlndiow',
            'groupId'=>'2'
        ],
        [
            'login'=>'PBi',
            'name'=>'Peter Bischoff',
            'department'=>'brak',
            'accessAdmin'=>'1',
            'email'=>'pbischoff@gmail.com',
            'password'=>'008lk2222ojwco',
            'groupId'=>'1'
        ]
        ,
        [
            'login'=>'cvO1976',
            'name'=>'Cornelius van Oyen',
            'department'=>'Hala 316 ',
            'accessAdmin'=>'1',
            'email'=>'corneliusvo@gmail.com',
            'password'=>'008lkjdjiojwco',
            'groupId'=>'3'
        ]
        ,
        [
            'login'=>'kfrey167998',
            'name'=>'Konrad Frey' ,
            'department'=>'Magazyn ',
            'accessAdmin'=>'1',
            'email'=>'kfrey@gmail.com',
            'password'=>'008lkjdjiojwco',
            'groupId'=>'3'
        ]
        ,
        [
            'login'=>'kremP3#l',
            'name'=>'Erich Krempel',
            'department'=>'Magazyn',
            'accessAdmin'=>'1',
            'email'=>'erichkrempell@gmail.com',
            'password'=>'00####3djio99)co',
            'groupId'=>'2'
        ]
        ,
        [
            'login'=>'FSch7',
            'name'=>'Fritz Schäfer',
            'department'=>'Magazyn ',
            'accessAdmin'=>'1',
            'email'=>'____uuuueffritzschaeffer@gmail.com',
            'password'=>'99966',
            'groupId'=>'1'
        ]]);
    
    }
}
