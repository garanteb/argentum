<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CostcenterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('costcenters')->insert([ [
            'name'=>'AUDI'
        ],
        [
            'name'=>'BMW'
        ],
        [
            'name'=>'VOLVO'
        ],
        [
            'name'=>'FIAT'
        ]
        ,
        [
            'name'=>'MAZDA'
        ],
        [
            'name'=>'HYUNDAI'
        ],
        [
            'name'=>'FORD'
        ],
        [
            'name'=>'RENAULT'
        ],
        [
            'name'=>'HONDA'
        ],
        [
            'name'=>'KIA'
        ],
        [
            'name'=>'SKODA'
        ],
        [
            'name'=>'TOYOTA'
        ],
        [
            'name'=>'MERCEDES'
        ],
        [
            'name'=>'NISSAN'
        ],
        [
            'name'=>'MITSUBISHI'
        ],
        [
            'name'=>'OPEL'
        ],
        [
            'name'=>'LAND ROVER'
        ],
        [
            'name'=>'ASTON MARTIN'
        ],
        [
            'name'=>'MAYBACH'
        ],
        [
            'name'=>'PORSCHE'
        ],
        [
            'name'=>'TESLA'
        ],
        [
            'name'=>'SUZUKI'
        ],
        [
            'name'=>'PONTIAC'
        ],
        [
            'name'=>'MINI'
        ],
        [
            'name'=>'MCLAREN'
        ],
        [
            'name'=>'LOTUS'
        ],
        [
            'name'=>'LEXUS'
        ],
        [
            'name'=>'SMART'
        ],
        [
            'name'=>'MASERATI'
        ],
        [
            'name'=>'LINCOLN'
        ]
        ]);
    }
}
