<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CubbySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cubbies')->insert([ [
            'name'=>'POJEMNIK 1'
        ],
        [
            'name'=>'POJEMNIK 2'
        ]
        ]);
    }
}
