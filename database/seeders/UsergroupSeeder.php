<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class UsergroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usergroups')->insert([ [
            'name'=>'magazyn'
        ],
        [
            'name'=>'ochrona'
        ],
        [
            'name'=>'spawalnia'
        ],
        [
            'name'=>'tokarnia'
        ],
        [
            'name'=>'cnc'
        ],
        [
            'name'=>'liderzy'
        ],
        [
            'name'=>'BHP'
        ],
        [
            'name'=>'narzędziownia'
        ],
        [
            'name'=>'huta'
        ],
        [
            'name'=>'lakiernia'
        ]
        
        
        ]);
    }
}
