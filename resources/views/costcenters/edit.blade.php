@extends('layouts/layout')
@section('content')     
    
       <h1>EDYCJA MPK</h1>    
       <div>@extends('layouts/createerror')</div>                                               
         <div>
         <form method="POST" action="{{ action('CostcenterController@update',[$costcenter->id]) }}" role="form"> 
            {{ csrf_field() }}
            {{ method_field('PUT') }}
                <div>
                    <label >NAZWA MPK</label>
                    <input type="text" name="name" value ="{{ $costcenter->name }}" >
                </div>
                <div>
                    <input type="submit" value="ZAPISZ">
                </div>
            </form>  
         </div>
            <a href =" {{ route('costcenters.index') }} "> WYJŚCIE </a>   
      
@endsection
