@extends('layouts/layout')
@section('content')     
    <h1>DODAWANIE NOWEGO CUBBY</h1>  
       <div>@extends('layouts/createerror')</div>                                                       
       <div class="container">
            <form method="POST" action="{{ action('CubbyController@store') }}" role="form"> 
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                    <label >NAZWA MPK</label>
                   <input type="text" name="name" placeholder="wpisz nazwę" value="{{ old('name') }}">
                </div>    
                <div>
                    <input type="submit" value="Dodaj">
                </div>
            </form>  
            <a href =" {{ route('cubbies.index') }}"> WYJŚCIE </a>   
        </div>   
@endsection
