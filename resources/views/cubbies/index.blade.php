@extends('layouts/layout')
@section('content')  
  <h1>CUBBY</h1>      
    <div class="container"> 
      <form method="POST" action="{{ action('CubbyController@search') }}" role="form"> 
        {{ csrf_field() }}
        <input class="typeahead form-control" type="text" name="name">
        <input type="submit" value=" SZUKAJ ">
      </form> 
    </div>

  <a href =" {{ route('cubbies.create') }}"> <img src="{{URL::asset('/img/create.png')}}" alt="create" height="20" width="20"> NOWY </a>                                                    
  @if (session('status'))
 <div class="alert alert-success">
  {{ session('status') }}
 </div>
  @endif
  
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Nazwa</th>
        <th>S</th>
        <th>E</th>
        <th>D</th>
      </tr>
    </thead>
    <tbody>
      @foreach($cubbiesList as $cubby)
        <tr>
          <td>{{$cubby->id}}</td>
          <td> {{$cubby->name}}</td>  
          <td><a href =" {{ route('cubbies.show',[$cubby->id]) }}"> <img src="{{URL::asset('/img/read.png')}}" alt="SHOW" height="20" width="20"> </a></td>  
          <td><a href =" {{ route('cubbies.edit',[$cubby->id]) }}"> <img src="{{URL::asset('/img/update.png')}}" alt="EDIT" height="20" width="20"> </a></td>  
          <td><a href =" {{ route('cubbies.delete',[$cubby->id]) }}"> <img src="{{URL::asset('/img/delete.png')}}" alt="DELETE" height="20" width="20"> </a></td> 
        </tr>
      @endforeach
    </tbody>
  </table>
<script type="text/javascript">
  var path = "{{ route('cubbies.autocomplete') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
                return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
</script>
@endsection