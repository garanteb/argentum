@extends('layouts/layout')
@section('content')     
    
       <h1>EDYCJA CUBBIE</h1>    
       <div>@extends('layouts/createerror')</div>                                               
         <div>
         <form method="POST" action="{{ action('CubbyController@update',[$cubby->id]) }}" role="form"> 
            {{ csrf_field() }}
            {{ method_field('PUT') }}
                <div>
                    <label >NAZWA CUBBY</label>
                    <input type="text" name="name" value ="{{ $cubby->name }}" >
                </div>
                <div>
                    <input type="submit" value="ZAPISZ">
                </div>
            </form>  
         </div>
            <a href =" {{ route('cubbies.index') }} "> WYJŚCIE </a>   
      
@endsection
