@extends('layouts/layout')
@section('content')     
    
       <h1>EDYCJA ARTYKUŁU</h1>    
        <div>@extends('layouts/createerror')</div>                                                
         <div>
         <form method="POST" action="{{ action('ItemController@update',[$item->id]) }}" role="form"> 
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div>
              
                    <label >NAZWA1</label>
                    <input type="text" name="name1" value="{{ $item->name1 }}"> <br>
              
                    <label >NAZWA2 </label>
                    <input type="text" name="name2" value="{{ $item->name2 }}"> <br>
                
                    <label >NAZWA3</label>
                    <input type="text" name="name3" value="{{ $item->name3 }}"> <br>
               
                    <label >BARCODE </label>
                    <input type="text" name="barcode" value="{{ $item->barcode }}"> <br>
   
                    <label >WYSOKOŚĆ </label>
                    <input type="text" name="height" value="{{ $item->height }}"> <br>

                    <label >SZEROKOŚĆ </label>
                    <input type="text" name="width" value="{{ $item->width }}"> <br>
          
                    <label >DŁUGOŚĆ </label>
                    <input type="text" name="length" value="{{ $item->length }}"> <br>
          
                    <label >WAGA </label>
                    <input type="text" name="weight" value="{{  $item->weight }}"> kg <br>
               
                    <label >OPAK ZBIORCZE</label>
                    <input type="text" name="boxAmount" placeholder="wpisz nazwę" value="{{ $item->boxAmount }}"> <br>
               
                    <label >STAN MIN.</label>
                    <input type="text" name="minInv" placeholder="wpisz nazwę" value="{{ $item->minInv }}"> <br>
               
                    <label >STAN AKTUALNY:</label>
                    {{ $item->currInv }}
                    <input type="hidden" name="currInv"  value="{{ $item->currInv }}"> <br>
                
                    <label >ZDJĘCIE </label>
                    <input type="text" name="picPath" placeholder="wpisz nazwę" value="{{ $item->picPath }}"> <br>
               
                    <label >DATA WAŻNOŚCI </label>
                    <input type="text" name="dateActive" placeholder="wpisz nazwę" value="{{ $item->dateActive }}"> <br>
              

                <input type="hidden" name="active" value="0">
                    @if ($item->active)
                    <input type="checkbox" id="active" name="active" value="1" checked>
                    @else
                    <input type="checkbox" id="active" name="active" value="1">
                    @endif
                    <label for="active"> ARTYKUŁ AKTYWNY </label><br>
        
            
                    <input type="submit" value="ZAPISZ">
                </div>
            </form>  
         </div>
            <a href =" {{ route('items.index') }} "> WYJŚCIE </a>   
      
@endsection
