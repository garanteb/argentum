@extends('layouts/layout')
@section('content')     

       <h1>DODAWANIE NOWEGO ARTYKUŁU</h1>  
       <div>@extends('layouts/createerror')</div>                                                        
       <div class="container">
            <form method="POST" action="{{ action('ItemController@store') }}" role="form"> 
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                <div> 
                    <label >NAZWA1</label>
                    <input type="text" name="name1" placeholder="wpisz nazwę" value="{{ old('name1') }}">
                </div>
                <div>
                    <label >NAZWA2 </label>
                    <input type="text" name="name2" placeholder="wpisz nazwę" value="{{ old('name2') }}">
                <div>
                <div>
                    <label >NAZWA3 </label>
                    <input type="text" name="name3" placeholder="wpisz nazwę" value="{{ old('name3') }}">
                <div>
                <div>
                    <label >BARCODE </label>
                    <input type="text" name="barcode" placeholder="wpisz barcode" value="{{ old('barcode') }}">
                <div>
                <div>
                    <label >WYSOKOŚĆ </label>
                    <input type="text" name="height" placeholder="wpisz wartość" value="{{ old('height') }}">
                </div>
                <div>
                    <label >SZEROKOŚĆ </label>
                    <input type="text" name="width" placeholder="wpisz wartość" value="{{ old('width') }}">
                </div>
                <div>
                    <label >DŁUGOŚĆ </label>
                    <input type="text" name="length" placeholder="wpisz wartość" value="{{ old('length') }}">
                </div>
                <div>
                    <label >WAGA </label>
                    <input type="text" name="weight" placeholder="wpisz wartość" value="{{ old('weight') }}">kg
                </div>
                    <label >OPAK ZBIORCZE</label>
                    <input type="text" name="boxAmount" placeholder="ilość w opak." value="{{ old('boxAmount') }}">
                </div>
                <div>
                    <label >STAN MIN.</label>
                    <input type="text" name="minInv" placeholder="stan min" value="{{ old('minInv') }}">
                    <input type="hidden" name="currInv" value="0">
                </div>
                <div>
                    <label >ZDJĘCIE </label>
                    <input type="text" name="picPath" placeholder="wpisz wartość" value="{{ old('picPath') }}">
                </div>
                <div>
                    <label >DATA WAŻNOŚCI </label>
                    <input type="text" name="dateActive" placeholder="wpisz wartość" value="{{ old('dateActive') }}">
                </div>
            
                <div>
                    <input type="hidden" name="active" value="0">
                    <input type="checkbox" id="active" name="active" value="1" checked>
                    <label for="active"> ARTYKUŁ AKTYWNY </label><br>
                </div>
                <div>
                    <input type="submit" value="Dodaj">
                </div>
            </form>  
            <a href =" {{ route('items.index') }}"> WYJŚCIE </a>   
        </div>   
@endsection
