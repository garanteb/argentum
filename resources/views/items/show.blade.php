@extends('layouts/layout')
@section('content')     

       <h1>WYBRANY ARTYKUŁ</h1>                                                           
         <div>
         <table>
            <tr><td>id</td><td> {{ $item->id }}</td></tr>
            <tr><td>nazwa1</td><td> {{ $item->name1 }}</td></tr>
            <tr><td>nazwa2</td><td> {{ $item->name2 }}</td></tr>
            <tr><td>nazwa3</td><td> {{ $item->name3 }}</td></tr>
            <tr><td>barcode</td><td> {{ $item->barcode }}</td></tr>
            <tr><td>wysokość</td><td> {{ $item->height }}</td></tr>  
            <tr><td>szerokość</td><td> {{ $item->width }}</td></tr> 
            <tr><td>długość</td><td> {{ $item->length }}</td></tr> 
            <tr><td>waga</td><td> {{ $item->weight }} kg</td></tr> 
            <tr><td>opak. zbiorcze</td><td> {{ $item->boxAmount }}</td></tr>
            <tr><td>stan min</td><td> {{ $item->minInv }}</td></tr>  
            <tr><td>stan aktualny</td><td> {{ $item->currInv }}</td></tr>  
            <tr><td>zdjęcie</td><td> {{ $item->picPath }}</td></tr> 
            <tr><td>data ważności</td><td> {{ $item->dateActive }}</td></tr> 
          
            @if($item->active)
            <tr><td>aktywny:</td><td> TAK </td></tr> 
            @else
            <tr><td>aktywny</td><td> NIE </td></tr> 
            @endif
            </table>
         </div>
            <a href =" {{ route('items.index') }}"> WYJŚCIE </a>   
      
@endsection
