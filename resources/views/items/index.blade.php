@extends('layouts/layout')
@section('content')  
  <h1>ARTYKUŁY</h1>      
    <div class="container"> 
      <form method="POST" action="{{ action('ItemController@search') }}" role="form"> 
        {{ csrf_field() }}
        <input class="typeahead form-control" type="text" name="name">
        <input type="submit" value=" SZUKAJ ">
      </form> 
    </div>
  <a href =" {{ route('padmin') }}"> PANEL ADMIN </a> <br> 
  <a href =" {{ route('items.create') }}"> <img src="{{URL::asset('/img/create.png')}}" alt="create" height="20" width="20"> NOWY </a>                                                    
  @if (session('status'))
 <div class="alert alert-success">
  {{ session('status') }}
 </div>
  @endif
  
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Nazwa1</th>
        <th>Nazwa2</th>
        <th>Nazwa3</th>
        <th>Barcode</th>
      </tr>
    </thead>
    <tbody>
      @foreach($itemsList as $item)
        <tr>
          <td>{{ $item->id }}</td>
          <td>{{ $item->name1 }}</td>
          <td>{{ $item->name2 }}</td>
          <td>{{ $item->name3 }}</td>
          <td>{{ $item->barcode }}</td> 
          <td><a href =" {{ route('items.show',[$item->id]) }}"> <img src="{{URL::asset('/img/read.png')}}" alt="SHOW" height="20" width="20"> </a></td>  
          <td><a href =" {{ route('items.edit',[$item->id]) }}"> <img src="{{URL::asset('/img/update.png')}}" alt="EDIT" height="20" width="20"> </a></td>  
          <td><a href =" {{ route('items.delete',[$item->id]) }}"> <img src="{{URL::asset('/img/delete.png')}}" alt="DELETE" height="20" width="20"> </a></td> 
        </tr>
      @endforeach
    </tbody>
  </table>
<script type="text/javascript">
  var path = "{{ route('items.autocomplete') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
                return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
</script>
@endsection