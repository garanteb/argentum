@extends('layouts/layout')
@section('content')     

       <h1>DODAWANIE NOWEJ GRUPY UŻYTKOWNIKÓW</h1>  
       <div>@extends('layouts/createerror')</div>                                                        
       <div class="container">
            <form method="POST" action="{{ action('UsergroupController@store') }}" role="form"> 
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                    <label >NAZWA GRUPY </label>
                    <input type="text" name="name" placeholder="wpisz nazwę" value="{{ old('name') }}">
                </div>
                <div>
                    <input type="submit" value="Dodaj">
                </div>
            </form>  
            <a href =" {{ route('usergroups.index') }}"> WYJŚCIE </a>   
        </div>   
@endsection
