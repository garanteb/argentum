@extends('layouts/layout')
@section('content')     

       <h1>USUWANIE GRUPY UŻYTKOWNIKÓW</h1>                                                           
         <div>
            <p> {{ $usergroup->name }} </p>
         </div>
         
            @if($userCount==0)
            <p>CZY USNUĄĆ REKORD?</p>
            
            <form method="POST" action="{{ action('UsergroupController@destroy',[$usergroup->id]) }}" role="form"> 
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
                <div>
                    <input type="submit" value="USUŃ">
                </div>
            </form>  


            @else

                    <!-- FRAGMENT DO POPRAWY 
                    @foreach($usergroup->user as $usrgr)
                    {{ $usrgr->name }}
                    @endforeach
                     -->

        <!-- ALTERNATYWA -->
        NIE MOŻNA USUNĄĆ GRUPY UŻYTKOWNIKÓW, PONIEWAŻ ISTNIEJĄ PRZYPISANIE DO NIEJ USERZY
        <div>
            <ul>
                @foreach($user as $usr)
                    <li> {{ $usr->name }} </li>
                @endforeach
            </ul>
        </div>

            @endif

            <a href =" {{ route('usergroups.index') }}"> WYJŚCIE </a>   
      
@endsection
