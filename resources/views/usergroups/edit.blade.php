@extends('layouts/layout')
@section('content')     
    
       <h1>EDYCJA GRUPY UŻYTKOWNIKÓW</h1>    
        <div>@extends('layouts/createerror')</div>                                            
         <div>
         <form method="POST" action="{{ action('UsergroupController@update',[$usergroup->id]) }}" role="form"> 
            {{ csrf_field() }}
            {{ method_field('PUT') }}
                <div>
                    <label >NAZWA GRUPY UŻYTKOWNIKA</label>
                    <input type="text" name="name" value ="{{ $usergroup->name}}" >
                </div>
                <div>
                    <input type="submit" value="ZAPISZ">
                </div>
            </form>  
         </div>
            <a href =" {{ route('usergroups.index') }} "> WYJŚCIE </a>   
      
@endsection
