@extends('layouts/layout')
@section('content')     

       <h1>USUWANIE MIEJSCA MAGAZYNOWEGO</h1>                                                           
         <div>
         <table>
            <tr><td>id</td><td> {{ $storageplace->id }}</td></tr>
            <tr><td>nazwa</td><td> {{ $storageplace->name }}</td></tr>
            <tr><td>barcode</td><td> {{ $storageplace->barcode }}</td></tr>  
            <tr><td>nr regału</td><td> {{ $storageplace->stillageNo }}</td></tr>
            <tr><td>nr poziomu</td><td> {{ $storageplace->shelfNo }}</td></tr>  
            <tr><td>nr miejsca</td><td> {{ $storageplace->placeNo }}</td></tr> 
            <tr><td>nr alejki</td><td> {{ $storageplace->lane }}</td></tr> 
            <tr><td>czas dostepu</td><td> {{ $storageplace->accessTime }}</td></tr> 
            <tr><td>wysokość</td><td> {{ $storageplace->height }}</td></tr>  
            <tr><td>szerokość</td><td> {{ $storageplace->width }}</td></tr> 
            <tr><td>długość</td><td> {{ $storageplace->lenght }}</td></tr> 
            <tr><td>maks.ładowność</td><td> {{ $storageplace->load }} kg</td></tr> 
            <tr><td>aktualne obciążenie</td><td> {{ $storageplace->loadCurrent }} kg</td></tr> 
            <tr><td>maks. liczba artykułów</td><td> {{ $storageplace->maxAmountOfItems }}</td></tr> 
         
            @if($storageplace->active)
            <tr><td>aktywny:</td><td> TAK </td></tr> 
            @else
            <tr><td>aktywny</td><td> NIE </td></tr> 
            @endif
            </table>
         </div>
            <p>CZY USNUĄĆ REKORD?</p>
            
            <form method="POST" action="{{ action('StorageplaceController@destroy',[$storageplace->id]) }}" role="form"> 
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
                <div>
                    <input type="submit" value="USUŃ">
                </div>
            </form>  


            <a href =" {{ route('storageplaces.index') }}"> WYJŚCIE </a>   
      
@endsection
