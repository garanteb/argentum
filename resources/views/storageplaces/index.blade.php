@extends('layouts/layout')
@section('content')  
  <h1>MIEJSCA MAGAZYNOWE</h1>      
    <div class="container"> 
      <form method="POST" action="{{ action('StorageplaceController@search') }}" role="form"> 
        {{ csrf_field() }}
        <input class="typeahead form-control" type="text" name="name">
        <input type="submit" value=" SZUKAJ ">
      </form> 
    </div>
  <a href =" {{ route('padmin') }}"> PANEL ADMIN </a> <br> 
  <a href =" {{ route('storageplaces.create') }}"> <img src="{{URL::asset('/img/create.png')}}" alt="create" height="20" width="20"> NOWY </a>                                                    
  @if (session('status'))
 <div class="alert alert-success">
  {{ session('status') }}
 </div>
  @endif
  
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Nazwa</th>
        <th>Barcode</th>
        <th>Nr regału</th>
        <th>Nr półki</th>
        <th>nr miejsca</th>
      </tr>
    </thead>
    <tbody>
      @foreach($storageplacesList as $storageplace)
        <tr>
          <td> {{ $storageplace->id }}</td>
          <td> {{ $storageplace->name }}</td>  
          <td> {{ $storageplace->barcode }}</td>  
          <td> {{ $storageplace->stillageNo }}</td>
          <td> {{ $storageplace->shelfNo }}</td>  
          <td> {{ $storageplace->placeNo }}</td>  
          <td><a href =" {{ route('storageplaces.show',[$storageplace->id]) }}"> <img src="{{URL::asset('/img/read.png')}}" alt="SHOW" height="20" width="20"> </a></td>  
          <td><a href =" {{ route('storageplaces.edit',[$storageplace->id]) }}"> <img src="{{URL::asset('/img/update.png')}}" alt="EDIT" height="20" width="20"> </a></td>  
          <td><a href =" {{ route('storageplaces.delete',[$storageplace->id]) }}"> <img src="{{URL::asset('/img/delete.png')}}" alt="DELETE" height="20" width="20"> </a></td> 
        </tr>
      @endforeach
    </tbody>
  </table>
<script type="text/javascript">
  var path = "{{ route('storageplaces.autocomplete') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
                return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
</script>
@endsection