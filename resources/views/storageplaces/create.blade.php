@extends('layouts/layout')
@section('content')     

       <h1>DODAWANIE NOWEGO MIEJSCA MAGAZYNOWEGO</h1>  
       <div>@extends('layouts/createerror')</div>                                                        
       <div class="container">
            <form method="POST" action="{{ action('StorageplaceController@store') }}" role="form"> 
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                <div> 
                    <label >NAZWA</label>
                    <input type="text" name="name" placeholder="wpisz nazwę" value="{{ old('name') }}">
                </div>
                <div>
                    <label >BARCODE </label>
                    <input type="text" name="barcode" placeholder="wpisz nazwę" value="{{ old('barcode') }}">
                <div>
                    <label >NR REGAŁU </label>
                    <input type="text" name="stillageNo" placeholder="wpisz nazwę" value="{{ old('stillageNo') }}">
                </div>
                <div>
                    <label >NR POZIOMU </label>
                    <input type="text" name="shelfNo" placeholder="wpisz nazwę" value="{{ old('shelfNo') }}">
                </div>
                <div>
                    <label >NR MIEJSCA </label>
                    <input type="text" name="placeNo" placeholder="wpisz nazwę" value="{{ old('placeNo') }}">
                </div>
                <div>
                    <label >NR ALEJKI </label>
                    <input type="text" name="lane" placeholder="wpisz nazwę" value="{{ old('lane') }}">
                <div>
                    <label >CZAS DOSTĘPU </label>
                    <input type="text" name="accessTime" placeholder="wpisz nazwę" value="{{ old('accessTime') }}">
                </div>
                <div>
                    <label >WYSOKOŚĆ </label>
                    <input type="text" name="height" placeholder="wpisz nazwę" value="{{ old('height') }}">
                </div>
                <div>
                    <label >SZEROKOŚĆ </label>
                    <input type="text" name="width" placeholder="wpisz nazwę" value="{{ old('width') }}">
                </div>
                <div>
                    <label >DŁUGOŚĆ </label>
                    <input type="text" name="length" placeholder="wpisz nazwę" value="{{ old('length') }}">
                </div>
                <div>
                    <label >MAKS. NOŚNOŚĆ </label>
                    <input type="text" name="loadMax" placeholder="wpisz nazwę" value="{{ old('loadMax') }}">
                    <input type="hidden" name="loadCurrent"  value="0">
                </div>
                <div>
                    <label >MAKS. LICZBA ARTYKUŁÓW </label>
                    <input type="text" name="maxAmountOfItems" placeholder="wpisz nazwę" value="{{ old('maxAmountOfItems') }}">
                </div>
                <div>
                    <input type="hidden" name="active" value="0">
                    <input type="checkbox" id="active" name="active" value="1" checked>
                    <label for="active"> REGAŁ AKTYWNY </label><br>
                </div>
                <div>
                    <input type="submit" value="Dodaj">
                </div>
            </form>  
            <a href =" {{ route('storageplaces.index') }}"> WYJŚCIE </a>   
        </div>   
@endsection
