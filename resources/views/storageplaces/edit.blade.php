@extends('layouts/layout')
@section('content')     
    
       <h1>EDYCJA MIEJSCA MAGAZYNOWEGO</h1>    
        <div>@extends('layouts/createerror')</div>                                                
         <div>
         <form method="POST" action="{{ action('StorageplaceController@update',[$storageplace->id]) }}" role="form"> 
            {{ csrf_field() }}
            {{ method_field('PUT') }}
                <div>
     
                    <label >NAZWA</label>
                    <input type="text" name="name" value="{{ $storageplace->name }}"> <br>
            
                    <label >BARCODE </label>
                    <input type="text" name="barcode"  value="{{ $storageplace->barcode }}"><br>
          
                    <label >NR REGAŁU </label>
                    <input type="text" name="stillageNo"  value="{{ $storageplace->stillageNo }}"><br>
              
                    <label >NR POZIOMU </label>
                    <input type="text" name="shelfNo" value="{{ $storageplace->shelfNo }}"><br>
            
                    <label >NR MIEJSCA </label>
                    <input type="text" name="placeNo"  value="{{ $storageplace->placeNo }}"><br>
             
                    <label >NR ALEJKI </label>
                    <input type="text" name="lane"  value="{{ $storageplace->lane }}"><br>
            
                    <label >CZAS DOSTĘPU </label>
                    <input type="text" name="accessTime"  value="{{ $storageplace->accessTime }}"><br>
             
                    <label >WYSOKOŚĆ </label>
                    <input type="text" name="height"  value="{{ $storageplace->height }}"><br>
            
                    <label >SZEROKOŚĆ </label>
                    <input type="text" name="length" value="{{ $storageplace->length }}"><br>
                   
                    <label >DŁUGOŚĆ </label>
                    <input type="text" name="width"  value="{{ $storageplace->width }}"><br>
            
                    <label >MAKS. OBCIĄŻENIE </label>
                    <input type="text" name="loadMax" value="{{ $storageplace->loadMax }}"><br>

                    <label >AKTUALNE. OBCIĄŻENIE </label>
                    {{ $storageplace->loadCurrent }}
                    <input type="hidden" name="loadCurrent" value="{{ $storageplace->loadCurrent }}"><br>
             
                    <label >MAKS. LICZBA RÓŻNYCH ARTYKUŁÓW </label>
                    <input type="text" name="maxAmountOfItems" value="{{ $storageplace->maxAmountOfItems }}"><br>

                    <input type="hidden" name="active" value="0">
                    @if ($storageplace->active)
                    <input type="checkbox" id="active" name="active" value="1" checked>
                    @else
                    <input type="checkbox" id="active" name="active" value="1">
                    @endif
                    <label for="active"> MIEJSCE MAGAZYNOWE AKTYWNE </label><br>
        
                    <input type="submit" value="ZAPISZ">
                </div>
            </form>  
         </div>
            <a href =" {{ route('storageplaces.index') }} "> WYJŚCIE </a>   
      
@endsection
