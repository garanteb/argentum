@extends('layouts/layout')
@section('content')  
  <h1>PANEL ADMINISTRACYJNY</h1>      
   <ul>
   <li> <a href =" {{ route('costcenters.index') }}"> MPK </a> </li>
   <li> <a href =" {{ route('usergroups.index') }}"> GRUPY UŻYTKOWNIKÓW </a> </li>
   <li> <a href =" {{ route('users.index') }}"> UŻYTKOWNICY </a>  </li>
   <li> <a href =" {{ route('items.index') }}"> ARTYKUŁY </a> </li>
   <li> <a href =" {{ route('storageplaces.index') }}"> MAGAZYN/REGAŁY </a> </li>
   <li> <a href =" {{ route('cubbies.index') }}"> POJEMNIKI </a> </li>
   <li> <a href =" {{ route('start') }}"> POWRÓT DO MENU GŁÓWNEGO </a> </li>
   </ul>
@endsection