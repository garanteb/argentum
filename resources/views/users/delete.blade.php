@extends('layouts/layout')
@section('content')     

       <h1>USUWANIE UŻYTKOWNIKA</h1>                                                           
         <div>
         <table>
            <tr><td>id</td><td> {{ $user->id }}</td></tr>
            <tr><td>nazwisko</td><td> {{ $user->name }}</td></tr>
            <tr><td>login</td><td> {{ $user->login }}</td></tr>  
            <tr><td>email</td><td> {{ $user->email }}</td></tr>
            <tr><td>hasło</td><td> {{ $user->password }}</td></tr>  
            <tr><td>wydział</td><td> {{ $user->department }}</td></tr> 
            <tr><td>id gr.userów</td><td> {{ $user->groupId }}</td></tr> 
            <tr><td>grupa userów</td><td> {{  $user->usergroups->name }}</td></tr> 
            @if($user->accessAdmin)
            <tr><td>dostęp administracyjny</td><td> TAK </td></tr> 
            @else
            <tr><td>dostęp administracyjny</td><td> NIE </td></tr> 
            @endif
            </table>
         </div>
            <p>CZY USUNĄĆ REKORD?</p>
            
            <form method="POST" action="{{ action('UserController@destroy',[$user->id]) }}" role="form"> 
            {{ csrf_field() }}
            {{ method_field('DELETE') }}
                <div>
                    <input type="submit" value="USUŃ">
                </div>
            </form>  


            <a href =" {{ route('users.index') }}"> WYJŚCIE </a>   
      
@endsection
