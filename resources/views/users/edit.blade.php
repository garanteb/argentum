@extends('layouts/layout')
@section('content')     
    
       <h1>EDYCJA UŻYTKOWNIKA</h1>    
        <div>@extends('layouts/createerror')</div>                                                
         <div>
         <form method="POST" action="{{ action('UserController@update',[$user->id]) }}" role="form"> 
            {{ csrf_field() }}
            {{ method_field('PUT') }}
                <div>
                    <label >NAZWA USERA</label>
                    <input type="text" name="name" value ="{{ $user->name }}" ><br>
                    <label >LOGIN</label>
                    <input type="text" name="login" value ="{{ $user->login }}" ><br>
                    <label >EMAIL</label>
                    <input type="text" name="email" value ="{{ $user->email }}" ><br>
                    <label >WYDZIAŁ</label>
                    <input type="text" name="department" value ="{{ $user->department }}" ><br>
                    <label >HASŁO</label>
                    <input type="text" name="password" value ="{{ $user->password }}" ><br>
                 
                    GRUPA USERÓW {{ $user->usergroups->name }} <br> 
                    <label >NOWA GRUPA USERÓW</label>
                    <select class="form-control" name="groupId">
                        @foreach($usergroups as $usergroup)
                        <option value="{{ $usergroup->id }}" {{  $user->groupId == $usergroup->id ? 'selected' : '' }} > {{ $usergroup->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <input type="hidden" name="accessAdmin" value="0">
                    @if ($user->accessAdmin)
                    <input type="checkbox" id="admin" name="accessAdmin" value="1" checked>
                    @else
                    <input type="checkbox" id="admin" name="accessAdmin" value="1">
                    @endif

                    <label for="accessAdmin"> DOSTĘP ADMINISTRACYJNY </label><br>
                </div>
                <div>
                    <input type="submit" value="ZAPISZ">
                </div>
            </form>  
         </div>
            <a href =" {{ route('users.index') }} "> WYJŚCIE </a>   
      
@endsection
