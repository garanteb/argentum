@extends('layouts/layout')
@section('content')     

       <h1>DODAWANIE NOWEGO UŻYTKOWNIKA</h1>  
       <div>@extends('layouts/createerror')</div>                                                        
       <div class="container">
            <form method="POST" action="{{ action('UserController@store') }}" role="form"> 
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div>
                <div> 
                    <label >NAZWA USERA </label>
                    <input type="text" name="name" placeholder="wpisz imię i nazwisko" value="{{ old('name') }}"> <br>
                </div>
                <div>
                    <label >LOGIN </label>
                    <input type="text" name="login" placeholder="wpisz login" value="{{ old('login') }}"> <br>
                </div>
                <div>
                    <label >WYDZIAŁ </label>
                    <input type="text" name="department" placeholder="wpisz wydział" value="{{ old('department') }}"> <br>
                </div>
                <div>
                    <label >EMAIL </label>
                    <input type="text" name="email" placeholder="wpisz email" value="{{ old('email') }}"> <br>
                </div>
                <div>
                    <label >HASŁO</label>
                    <input type="text" name="password" placeholder="wpisz hasło" value="{{ old('password') }}" ><br>
                </div>    
                <div> 
                    <label >WYBIERZ GRUPĘ UŻYTKOWNIKÓW:</label>
                    <select class="form-control" name="groupId">
                        @foreach($usergroups as $usergroup)
                        <option value="{{$usergroup->id}}">{{ $usergroup->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <input type="hidden" name="accessAdmin" value="0">
                    <input type="checkbox" id="admin" name="accessAdmin" value="1" checked>
                    <label for="accessAdmin"> DOSTĘP ADMINISTRACYJNY </label><br>
                </div>
                <div>
                    <input type="submit" value="Dodaj">
                </div>
            </form>  
            <a href =" {{ route('users.index') }}"> WYJŚCIE </a>   
        </div>   
@endsection
