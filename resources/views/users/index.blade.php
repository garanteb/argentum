@extends('layouts/layout')
@section('content')  
  <h1>UŻYTKOWNICY</h1>      
    <div class="container"> 
      <form method="POST" action="{{ action('UserController@search') }}" role="form"> 
        {{ csrf_field() }}
        <input class="typeahead form-control" type="text" name="name">
        <input type="submit" value=" SZUKAJ ">
      </form> 
    </div>
  <a href =" {{ route('padmin') }}"> PANEL ADMIN </a> <br>  
  <a href =" {{ route('users.create') }}"> <img src="{{URL::asset('/img/create.png')}}" alt="create" height="20" width="20"> NOWY </a>                                                    
  @if (session('status'))
 <div class="alert alert-success">
  {{ session('status') }}
 </div>
  @endif
  
  <table class="table">
    <thead>
      <tr>
        <th>#</th>
        <th>Nazwa</th>
        <th>Grupa Userów</th>
      </tr>
    </thead>
    <tbody>
      @foreach($usersList as $user)
        <tr>
          <td>{{$user->id}}</td>
          <td> {{$user->userName}}</td>  
          <td> {{$user->usergroupName}}</td>  


          <td><a href =" {{ route('users.show',[$user->id]) }}"> <img src="{{URL::asset('/img/read.png')}}" alt="SHOW" height="20" width="20"> </a></td>  
          <td><a href =" {{ route('users.edit',[$user->id]) }}"> <img src="{{URL::asset('/img/update.png')}}" alt="EDIT" height="20" width="20"> </a></td>  
          <td><a href =" {{ route('users.delete',[$user->id]) }}"> <img src="{{URL::asset('/img/delete.png')}}" alt="DELETE" height="20" width="20"> </a></td> 
        </tr>
      @endforeach
    </tbody>
  </table>
<script type="text/javascript">
  var path = "{{ route('users.autocomplete') }}";
        $('input.typeahead').typeahead({
            source:  function (query, process) {
                return $.get(path, { query: query }, function (data) {
                    return process(data);
                });
            }
        });
</script>
@endsection